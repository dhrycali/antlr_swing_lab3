tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
}
prog    : (e+=expr | e+=bloc | d+=decl)* 
        -> template(name={$e},deklaracje={$d}) "<deklaracje> start:<\"\n\"> <name;separator=\"\n\"> ";

decl  :
        ^(VAR i1=ID) {locals.newSymbol($ID.text);} -> dek(n={locals.getLocal($ID.text)})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> add(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> sub(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> mul(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> div(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr) {locals.isDec($i1.text);} -> set(n={locals.getLocal($i1.text)},p={$e2.st})
        | ID {locals.isDec($ID.text);} -> get(n={locals.getLocal($ID.text)})
        | INT  {numer++;}                    -> int(i={$INT.text},j={numer.toString()})
        | ^(IF e1=expr e2=expr e3=expr?) {numer++;} 
          -> if(p1={$e1.st},p2={$e2.st},p3={$e3.st}, n={numer.toString()})
        | ^(EQ e1=expr e2=expr) {numer++;} -> eq(p1={$e1.st},p2={$e2.st}, n={numer.toString()})
        | ^(NOT_EQ e1=expr e2=expr) {numer++;} -> notEq(p1={$e1.st},p2={$e2.st}, n={numer.toString()})
    ;

bloc    : ^(LB {locals.enterScope();} (e+=bloc | e+=expr | d+=decl)* {locals.leaveScope();}) 
          -> template(name={$e},deklaracje={$d}) "<deklaracje> <name;separator=\"\n\"> ";
    